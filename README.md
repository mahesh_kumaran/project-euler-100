# ProjectEuler100

Project Euler is a website created back in 2001. It hosts a collection of around 600 different algorithm problems that get progressively harder, to the point where even people with math PhD's still struggle with them.

This said, the first 100 problems are totally do-able by a developer. Thousands of people have completed the first 100 Project Euler problems over the years.

The challenge is named after Leonhard Euler, one of the most prolific mathematicians in history.

Now taking my turn to solve them out!

