'''' 
Project Euler: Problem 1: Multiples of 3 and 5

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below the provided parameter value number.

python-3.6 : Mahesh Kumaran - maheshtkumaran@gmail.com - 09th January 2020
''''

def multiples_of_3_5(number):
	sum = 0
	for num in range(0,number):
		if num % 3 == 0 or num % 5 == 0:
			sum =  sum + num
	return sum

print(multiples_of_3_5(10))



